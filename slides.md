# How to use network namespaces to avoid leaking IP adresses in VPNs
## Laurent Fasnacht (fasnacht@protonmail.ch)

Slides available here: https://lfasnacht.gitlab.io/slides-netns/

---

# About me...

* Bachelor in Mathematics
* Master in Computational Science
* PhD in hydrogeology :-)
* Now: sysadmin at Proton Technologies AG (mostly on ProtonVPN)

---

# What I'm working on now at Proton

* Dynamic routing for specific services
* Internal PKI
* Remote encryption of servers (https://protonvpn.com/blog/disk-encryption/)
* Various other projects...

---

# What is a VPN?

* Encrypted tunnel between two machines (usually UDP)
* ProtonVPN: IKEv2 and OpenVPN 
* Traffic exits in clear at the server

---

# Why use a VPN?

* Protect your data exchange from sniffing
* Hide your IP
* Bypassing geo-restrictions (e.g. Netflix US)

---

# In practice (OpenVPN)

Before:

```shell
$ ip route
default via 10.0.2.1 dev wlp2s0 proto dhcp metric 600 
10.0.2.0/24 dev wlp2s0 proto kernel scope link src 10.0.2.79 metric 600 
169.254.0.0/16 dev wlp2s0 scope link metric 1000 
```

Connected:

```shell
$ ip route
default via 10.8.0.1 dev tun0 proto static metric 50 
default via 10.0.2.1 dev wlp2s0 proto dhcp metric 600 
10.0.2.0/24 dev wlp2s0 proto kernel scope link src 10.0.2.79 metric 600 
10.0.2.1 dev wlp2s0 proto static scope link metric 600 
10.8.0.0/24 dev tun0 proto kernel scope link src 10.8.0.4 metric 50 
169.254.0.0/16 dev wlp2s0 scope link metric 1000 
185.159.157.8 via 10.0.2.1 dev wlp2s0 proto static metric 600 
```

---

# StrongSwan (IKEv2)

Before:

```shell
$ ip route
default via 10.0.2.1 dev wlp2s0 proto dhcp metric 600 
10.0.2.0/24 dev wlp2s0 proto kernel scope link src 10.0.2.79 metric 600 
169.254.0.0/16 dev wlp2s0 scope link metric 1000 
```

Connected:

```shell
default via 10.0.2.1 dev wlp2s0 proto dhcp metric 600 
10.0.2.0/24 dev wlp2s0 proto kernel scope link src 10.0.2.79 metric 600 
10.6.6.149 dev wlp2s0 proto kernel scope link src 10.6.6.149 metric 50 
10.6.6.149 dev wlp2s0 proto kernel scope link src 10.6.6.149 metric 600 
169.254.0.0/16 dev wlp2s0 scope link metric 1000 
```

## ???

---

# IKEv2

* Uses kernel IPSec

```shell
$ setkey -PD
10.6.6.49[any] 0.0.0.0/0[any] 255
	out prio high + 1073358209 ipsec
	esp/tunnel/10.0.2.79-185.159.157.8/unique:2
	created: May 11 09:07:32 2019  lastused: May 11 09:08:55 2019
	lifetime: 0(s) validtime: 0(s)
	spid=297 seq=4 pid=16756
	refcnt=2
0.0.0.0/0[any] 10.6.6.49[any] 255
	in prio high + 1073358209 ipsec
	esp/tunnel/185.159.157.8-10.0.2.79/unique:2
	created: May 11 09:07:32 2019  lastused: May 11 09:08:55 2019
	lifetime: 0(s) validtime: 0(s)
	spid=280 seq=6 pid=16756
	refcnt=1

$ ip rule
0:	from all lookup local 
220:	from all lookup 220 
32766:	from all lookup main 
32767:	from all lookup default 
$ ip route show table 220
default via 10.0.2.1 dev wlp2s0 proto static src 10.6.6.149 
10.0.2.0/24 dev wlp2s0 proto static src 10.0.2.79 
```

---

# So we're fine?

---

# Not really :-(

* IPv6

```shell
$ ip -6 route
::1 dev lo proto kernel metric 256 pref medium
2a02:aa13:8105:1680::/64 via fe80::5667:51ff:fec3:8a9b dev wlp2s0 proto ra metric 600 pref medium
2a02:aa13:8105:1680::/64 dev wlp2s0 proto kernel metric 600 pref medium
fe80::/64 dev wlp2s0 proto kernel metric 600 pref medium
default via fe80::5667:51ff:fec3:8a9b dev wlp2s0 proto ra metric 600 pref high
```

* What about applications?

---

# WebRTC Leak

Try it: https://webrtc-test.protonvpn.com/

![webrtc-leak](webrtc-leak.png)

---

# How to solve?

* Disable IPv6?
* Disable WebRTC?
* What about other applications?
* Raw sockets?

---

# Namespaces

* Like containers, but specific to a given aspect
* Try: `lsns`

```
        NS TYPE   NPROCS   PID USER             COMMAND
4026531835 cgroup    191     1 root             /sbin/init
4026531836 pid       191     1 root             /sbin/init
4026531837 user      191     1 root             /sbin/init
4026531838 uts       191     1 root             /sbin/init
4026531839 ipc       191     1 root             /sbin/init
4026531840 mnt       179     1 root             /sbin/init
4026531860 mnt         1    20 root             kdevtmpfs
4026531992 net       190     1 root             /sbin/init
4026532237 mnt         1   331 root             /lib/systemd/systemd-udevd
4026532364 mnt         1   471 systemd-timesync /lib/systemd/systemd-timesyncd
4026532365 mnt         1   518 root             /usr/sbin/ModemManager --filter-policy=strict
4026532366 mnt         1   523 root             /usr/lib/bluetooth/bluetoothd
4026532367 mnt         3   527 root             /usr/sbin/NetworkManager --no-daemon
4026532425 net         1   648 rtkit            /usr/lib/rtkit/rtkit-daemon
4026532478 mnt         1   648 rtkit            /usr/lib/rtkit/rtkit-daemon
4026532535 mnt         1   739 root             /usr/lib/upower/upowerd
4026532537 mnt         1   861 colord           /usr/lib/colord/colord
4026532539 mnt         1  1251 root             /usr/lib/bolt/boltd
```

---

# Create your own network namespace

```shell

# ip link
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: wlp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP mode DORMANT group default qlen 1000
    link/ether 40:a3:cc:0d:52:6e brd ff:ff:ff:ff:ff:ff
# unshare -n /bin/bash
# ip link
1: lo: <LOOPBACK> mtu 65536 qdisc noop state DOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
# ls /sys/class/net/
lo  wlp2s0
# 
```

* We also need to handle mounts...

---

# What we want in our netns

* `systemd-udevd`
* `NetworkManager`
* `mount /sys`
* a fresh `/etc/resolv.conf`

Systemd should be able to do it, but unfortunately needs help.

`/etc/systemd/system/untrusted-netns.conf`:

```ini
[Unit]
Description=Untrusted network namespace
After=network-pre.target
Before=network.target network-online.target

[Install]
WantedBy=network-online.target
WantedBy=multi-user.target

[Service]
Type=simple
PrivateNetwork=yes
PrivateMounts=yes

ExecStart=/bin/sh -c "/bin/mount -t sysfs sysfs /sys; mkdir /run/untrusted-netns /run/untrusted-netns/etc-upper /run/untrusted-netns/etc-work; mount -t overlay -o lowerdir=/etc,upperdir=/run/untrusted-netns/etc-upper,workdir=/run/untrusted-netns/etc-work overlay /etc; echo $$$ > /run/untrusted-netns.pid; exec /lib/systemd/systemd-udevd"
ExecStop=/bin/rm -rf /run/untrusted-netns.pid /run/untrusted-netns
```

---

# Checking that the ns is working

```shell
nsenter -t `cat /run/untrusted-netns.pid` -n -m
```

---

# Moving the physical devices to the netns

`/etc/udev/rules.d/99untrusted-netns.rules`:
```ini
SUBSYSTEM=="net", ACTION=="add", DEVPATH!="/devices/virtual/*", TAG+="systemd", ENV{SYSTEMD_WANTS}="untrusted-netns-device@.service"
```

`/etc/systemd/system/untrusted-netns-device@.service`:

```ini
[Unit]
Description=Move interface to untrusted network namespace
Requires=untrusted-netns.service
After=untrusted-netns.service

[Service]
Type=oneshot
ExecStart=/bin/sh -c "if [ -L /%I/phy80211 ]; then /sbin/iw phy `basename \`readlink /%I/phy80211\`` set netns `cat /run/untrusted-netns.pid`; else /bin/ip link set `basename /%I` netns `cat /run/untrusted-netns.pid`; fi"
```

Run:
```shell
$ sudo udevadm control -R
$ sudo udevadm trigger -c add -s net -v
```

---

# Moving NetworkManager

`/etc/systemd/system/NetworkManager.service.d/untrusted-netns.conf`

```ini
[Unit]
BindsTo=untrusted-netns.service
After=untrusted-netns.service

[Service]
ExecStart=
ExecStart=/bin/sh -c "exec /usr/bin/nsenter -t `cat /run/untrusted-netns.pid` -n -m /usr/sbin/NetworkManager --no-daemon"
CapabilityBoundingSet=~
```

`/etc/systemd/system/NetworkManager-dispatcher.service.d/untrusted-netns.conf`
```ini
[Unit]
BindsTo=untrusted-netns.service
After=untrusted-netns.service

[Service]
ExecStart=
ExecStart=/bin/sh -c "exec /usr/bin/nsenter -t `cat /run/untrusted-netns.pid` -n -m /usr/lib/NetworkManager/nm-dispatcher"


```

---

# Moving `wpa_supplicant`

`/etc/systemd/system/wpa_supplicant.service.d/untrusted-netns.conf`
```ini
[Unit]
BindsTo=untrusted-netns.service
After=untrusted-netns.service

[Service]
ExecStart=
ExecStart=/bin/sh -c "exec /usr/bin/nsenter -t `cat /run/untrusted-netns.pid` -n -m /sbin/wpa_supplicant -u -s -O /run/wpa_supplicant"
```

---

# Move the vpn interface to the main netns

`/etc/NetworkManager/dispatcher.d/00-move-netns`:

```shell
#!/bin/sh -e

test `readlink /proc/1/ns/net` = `readlink /proc/self/ns/net` && exit 0

case "$2" in
	vpn-up)
		printenv > /tmp/out.env
		/bin/ip link set $1 netns 1 up
		/usr/bin/nsenter -t 1 -a /bin/ip addr add `echo $VPN_IP4_ADDRESS_0 | awk '{print $1;}'` dev $1
		/usr/bin/nsenter -t 1 -a /bin/ip route add 0.0.0.0/0 via $VPN_IP4_GATEWAY
		for ns in $VPN_IP4_NAMESERVERS; do
			echo "nameserver $ns" >> /tmp/vpn-resolv.conf
		done

		/usr/bin/nsenter -t 1 -a /bin/mv /tmp/vpn-resolv.conf /etc/resolv.conf
		;;
	vpn-down)
		;;
esac
```

---

# Limitations

* Not easy to setup (unfortunately)
* Cannot use systemd `JoinsNamespaceOf`
* Only works with VPN that create an interface (not IKEv2)
* Dispatch script is very limited...

---

# Any questions?

---


# We're hiring!

* https://protonmail.com/careers
* You can also contact me directly

---

# Goodies time :-)

